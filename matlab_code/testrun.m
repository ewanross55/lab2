load ("rice_dataset");

randomisedData = RiceOsmancikCammeoDataset(randperm(size(RiceOsmancikCammeoDataset,1)),:);

%% Normalisation and regulation
outliersFilled = filloutliers(randomisedData(:,1:7),'clip','percentiles',[5 95]);

%% NORMALISATION
Normalized = normalize(outliersFilled,'range');
fullData = [Normalized, randomisedData(:,8)];

%% Splitting Data
%testing sets
%variables to set data amounts for testing
TrainingAmount = 1/3;
InitialTesting = 1/3;
FinalTesting = 1/3;

tableSize = size(fullData,1); %number of datapoints in total

%creating tables with correct data

%we only use training and testingdata giving us a 50-50 training testing
%split
TrainingData = fullData(1:(TrainingAmount * tableSize),:) ;
TestingData = fullData(  ((TrainingAmount * tableSize)+1): (2*(TrainingAmount * tableSize)) ,:);
FinalData = fullData( ((2*(TrainingAmount * tableSize))+1):tableSize, :);

%% obtain features and labels
X = TrainingData(:,1:end-1);
Y = TrainingData(:,end);

% We decided to make Cammeo positive, and Osmancik negative
% remainderArray = EntropyRemainder(p,n,X,Y)

[best_att, best_thrsh] = runChooseAttribute(TrainingData, X,Y)






