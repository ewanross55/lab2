load ("rice_dataset");

randomisedData = RiceOsmancikCammeoDataset(randperm(size(RiceOsmancikCammeoDataset,1)),:);

%% Normalisation and regulation
outliersFilled = filloutliers(randomisedData(:,1:7),'clip','percentiles',[5 95]);

%% NORMALISATION
Normalized = normalize(outliersFilled,'range');
fullData = [Normalized, randomisedData(:,8)];

%% Splitting Data
%testing sets
%variables to set data amounts for testing
TrainingAmount = 1/3;
InitialTesting = 1/3;
FinalTesting = 1/3;

tableSize = size(fullData,1); %number of datapoints in total

%creating tables with correct data

%we only use training and testingdata giving us a 50-50 training testing
%split
TrainingData = fullData(1:(TrainingAmount * tableSize),:) ;
TestingData = fullData(  ((TrainingAmount * tableSize)+1): (2*(TrainingAmount * tableSize)) ,:);
FinalData = fullData( ((2*(TrainingAmount * tableSize))+1):tableSize, :);

%% obtain features and labels
X = TrainingData(:,1:end-1);
Y = TrainingData(:,end);

[p,n] = calcfullPN(TrainingData(:,1), Y);
p;
n;
% We decided to make Cammeo positive, and Osmancik negative
% remainderArray = EntropyRemainder(p,n,X,Y)

[best_att, best_thrsh] = runChooseAttribute(TrainingData, p,n,X,Y)

%% calcPN
% function to find the amount of positives and negatives in a specific
% feature
function [positive, negative] = calcPN(feature)
positive = 0;
negative = 0;
Z = feature.CLASS;
    for i = 1:size(feature)
        if(Z(i,:) == "Cammeo")
            positive = positive + 1;
        else 
            negative = negative + 1;
        end 
    end
end

%% caclfullPN
% calculating the amount of positives and negatives in the entire dataset
function [positive, negative] = calcfullPN(feature, Y)
    positive = 0;
    negative = 0;
    Z = Y.CLASS;
    
    for i = 1:size(feature)
        if(Z(i,:) == "Cammeo")
            positive = positive + 1;
        else 
            negative = negative + 1;
        end 
    end
end

%% calcEntropy
%This would calculate the entropy based on the number of positives and
%negatives given
function output = calcEntropy(p, n)
    
    output = -(p/(p+n)) * log2( p/(p+n) ) - (n/(p+n)) * log2(n/(p+n));
    
end
  
%% EntropyRemainder
%This loops through each feature and splits it into smaller sorted subsets. These
%smaller subsets will be tested. A remainder will be obtained from each
%feature
function remainderArray = EntropyRemainder(p, n, X, Y, threshold)
    remainderArray = {};
    
    number_of_subsets = 2;
        
    %for each feature
     for feature_index = 1:size(X,2)
        remainder = 0;
        
        %fprintf("\nChecking attribute: %s", X.Properties.VariableNames{feature_index});
        
        feature_values = X(:,feature_index);
        
        sortedFeature = sortFeature(feature_values, Y);
        
        featureSize = size(sortedFeature,1);
        %for each subset of the chosen feature
%         for count=1:number_of_subsets
            startIndexa = 1;
            finishIndexa = threshold;
            
            startIndexb = threshold + 1;
            finishIndexb = featureSize;

            partialFeaturea = sortedFeature(startIndexa:finishIndexa,:);
            partialFeatureb = sortedFeature(startIndexb:finishIndexb,:);

            [pIa,nIa] = calcPN(partialFeaturea);
            [pIb,nIb] = calcPN(partialFeatureb);
            
            remainderIa = ((pIa + nIa)/(featureSize)) * calcEntropy(pIa, nIa);
            remainderIb = ((pIb + nIb)/(featureSize)) * calcEntropy(pIb, nIb);
            
%             fprintf("\n p: %d n: %d", pIa, nIa);
%             fprintf("\n p: %d n: %d", pIb, nIb);
%            fprintf("\n first half:%f second half:%f \n", ((pI + nI)/(featureSize)), calcEntropy(pI, nI));
            
%           if isnan(remainderI)
%                remainderI = 0;
%           end
            
            remainder = remainderIa + remainderIb;
%         end
         
            remainderArray{feature_index} = remainder;
    end
    
    for i = 1:size(remainderArray)
        remainderArray{i};
    end
end

%% EntropyGain
%This function calculates the gain for each attribute and returns the
%attribute with the highest gain.
function [gain, chosenAttributeIndex] = EntropyGain(p, n, X, Y, threshold)
    highestGain = 0;
    currentGain = 0;
    chosenAttributeIndex = 0;
    remainderArray = EntropyRemainder(p, n, X, Y, threshold);

    for i = 1:size(remainderArray, 2)
        
        currentGain = calcEntropy(p, n) - remainderArray{i};
        
        %fprintf("\n currentGain: %f, currentAttribute: %s \n", currentGain, X.Properties.VariableNames{i});
        
        if(currentGain > highestGain)
            highestGain = currentGain;
            chosenAttributeIndex = i;
        end
    end
    gain = highestGain;
    %fprintf("\n gain: %f", gain);
    
    
end

%% sortFeature 
%This function sorts the table by a certain feature in ascending order
function out = sortFeature(feature, Y)
    newTable = [feature, Y];
    out = sortrows(newTable);
end 


%% hillClimb
function best_threshold = hillClimb(size, p, n, X, Y)
        thrsh_Array = {};
        best_threshold = 0;
        flag = true;
        threshold_value = 0;
        
        for feature_index = 1:7
            for i = 1:10
                test_threshold = randi(size);

                currentGain = EntropyGain(p, n, X(:,feature_index), Y, test_threshold);

                while(flag == true)

                    Gainplus = EntropyGain(p, n, X(:,feature_index), Y, test_threshold + 1);
                    GainMinus = EntropyGain(p, n, X(:,feature_index), Y, test_threshold - 1);

                    if(Gainplus > GainMinus && Gainplus >= currentGain)
                        currentGain = Gainplus;
                        test_threshold = test_threshold + 1;
                    elseif(Gainplus < GainMinus && GainMinus >= currentGain)
                        currentGain = GainMinus;
                        test_threshold = test_threshold - 1;
                    end

                    if(Gainplus <= currentGain && GainMinus <= currentGain)
                        flag = false;
                    end
                end

                if(currentGain > threshold_value)
                    threshold_value = currentGain;
                    best_threshold = test_threshold;
                end
            end
            %thrsh_Array{feature_index} = best_threshold;
       end
end

%% chooseAttribute

function [Best_Attribute, Best_Threshold] = runChooseAttribute(TrainingData, p,n,X,Y)

    Best_Threshold = hillClimb(size(TrainingData,1), p, n, X, Y);
    [Best_Gain, Best_Attribute_Index] = EntropyGain(p,n,X,Y, Best_Threshold);

    Best_Attribute = X.Properties.VariableNames{Best_Attribute_Index};
    
end
