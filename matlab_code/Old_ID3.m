load ("rice_dataset");

randomisedData = RiceOsmancikCammeoDataset(randperm(size(RiceOsmancikCammeoDataset,1)),:);

%% Normalisation and regulation
outliersFilled = filloutliers(randomisedData(:,1:7),'clip','percentiles',[5 95]);

%% NORMALISATION
Normalized = normalize(outliersFilled,'range');
fullData = [Normalized, randomisedData(:,8)];


%% Splitting Data
%testing sets
%variables to set data amounts for testing
TrainingAmount = 1/3;
InitialTesting = 1/3;
FinalTesting = 1/3;

tableSize = size(fullData,1); %number of datapoints in total

%creating tables with correct data

%we only use training and testingdata giving us a 50-50 training testing
%split
TrainingData = fullData(1:(TrainingAmount * tableSize),:) ;
TestingData = fullData(  ((TrainingAmount * tableSize)+1): (2*(TrainingAmount * tableSize)) ,:);
FinalData = fullData( ((2*(TrainingAmount * tableSize))+1):tableSize, :);
%% obtain features and labels
X = TrainingData(:,1:end-1);
Y = TrainingData(:,end);

[p,n] = calcfullPN(TrainingData(:,1), Y);
p;
n;
% We decided to make Cammeo positive, and Osmancik negative
% remainderArray = EntropyRemainder(p,n,X,Y)
ChosenAttribute = EntropyGain(p,n,X,Y)

%% calcPN
% function to find the amount of positives and negatives in a specific
% feature
function [positive, negative] = calcPN(feature)
positive = 0;
negative = 0;
Z = feature.CLASS;
    for i = 1:size(feature)
        if(Z(i,:) == "Cammeo")
            positive = positive + 1;
        else 
            negative = negative + 1;
        end 
    end
end

%% caclfullPN
% calculating the amount of positives and negatives in the entire dataset
function [positive, negative] = calcfullPN(feature, Y)
    positive = 0;
    negative = 0;
    Z = Y.CLASS;
    
    for i = 1:size(feature)
        if(Z(i,:) == "Cammeo")
            positive = positive + 1;
        else 
            negative = negative + 1;
        end 
    end
end

%% calcEntropy
%This would calculate the entropy based on the number of positives and
%negatives given
function output = calcEntropy(p, n)
    
    output = -(p/(p+n)) * log2( p/(p+n) ) - (n/(p+n)) * log2(n/(p+n));
    
end
  
%% EntropyRemainder
%This loops through each feature and splits it into smaller sorted subsets. These
%smaller subsets will be tested. A remainder will be obtained from each
%feature
function remainderArray = EntropyRemainder(p, n, X, Y)
    remainderArray = {};
    
    number_of_subsets = 2;
        
    %for each feature
     for feature_index = 1:size(X,2)
        remainder = 0;
        
        %fprintf("\nChecking attribute: %s", X.Properties.VariableNames{feature_index});
        
        feature_values = X(:,feature_index);
        
        sortedFeature = sortFeature(feature_values, Y);
        
        featureSize = size(sortedFeature,1);
        %for each subset of the chosen feature
        for count=1:number_of_subsets
            startIndex = floor((count - 1) * featureSize/number_of_subsets) + 1;
            finishIndex = floor((count * (featureSize/number_of_subsets)));

            partialFeature = sortedFeature(startIndex:finishIndex,:);

            [pI,nI] = calcPN(partialFeature);

            remainderI = ((pI + nI)/(featureSize)) * calcEntropy(pI, nI);
            
            %fprintf("\n p: %d n: %d", pI, nI);
            %fprintf("\n first half:%f second half:%f \n", ((pI + nI)/(featureSize)), calcEntropy(pI, nI));
            
%           if isnan(remainderI)
%                remainderI = 0;
%           end
            
            remainder = remainder + remainderI;
        end
         
            remainderArray{feature_index} = remainder;
    end
    
    for i = 1:size(remainderArray)
        remainderArray{i};
    end
end

%% EntropyGain
%This function calculates the gain for each attribute and returns the
%attribute with the highest gain.
function chosenAttribute = EntropyGain(p, n, X, Y)
    highestGain = 0;
    currentGain = 0;
    chosenAttributeIndex = 0;
    remainderArray = EntropyRemainder(p, n, X, Y);

    for i = 1:size(remainderArray, 2)
        
        currentGain = calcEntropy(p, n) - remainderArray{i};
        
        %fprintf("\n currentGain: %f, currentAttribute: %s \n", currentGain, X.Properties.VariableNames{i});
        
        if(currentGain > highestGain)
            highestGain = currentGain;
            chosenAttributeIndex = i;
        end
    end
    highestGain
    chosenAttribute = X.Properties.VariableNames{chosenAttributeIndex};
    
end

%% sortFeature 
%This function sorts the table by a certain feature in ascending order
function out = sortFeature(feature, Y)
    newTable = [feature, Y];
    out = sortrows(newTable);
end 
