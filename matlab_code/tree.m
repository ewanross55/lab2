%% MATLAB TREE STRUCTURE
%{

 The resulting tree must be a MATLAB 
structure (struct) with the following fields:
� tree.op : a label name (string) for the corresponding node (i.e. the name of the 
attribute that the node is testing). It must be an empty string for a leaf node.
� tree.kids : a cell array which will contain the subtrees that initiate from the 
corresponding node. Since the resulting tree will be binary, the size of this cell 
array must be 1x2, where the entries will contain the left and right subtrees 
respectively. This must be empty for a leaf node since a leaf has no kids, i.e. 
tree.kids = [].
� tree.prediction : a label for the predicted class/regression value. This field can 
have the following possible values:
o {0, 1}: for classification trees, or a real value for regression trees.
o It must be empty for an internal node, since the tree returns a label only 
in a leaf node.
� tree.attribute : the attribute number tested at this node
� tree.threshold : the threshold for the attribute to decide which way to send data 
points


%}

T = struct( 'op', '', 'kids', ([]*2),  'prediction', 0.00, 'attribute', 0, 'threshold', 0);