load('rice_dataset.mat');

%% Initialising data


%the data starts off with one class in the first half, the the next class,
%so we randomise to allow training with both classes
randomisedData = RiceOsmancikCammeoDataset(randperm(size(RiceOsmancikCammeoDataset,1)),:);


%% Normalisation and regulation
outliersFilled = filloutliers(randomisedData(:,1:7),'clip','percentiles',[5 95]);

%drawing a boxplot
%{
figure;
subplot(3,3,1);
boxplot(OutliersFilled.AREA);
title('Area');
subplot(3,3,2);
boxplot(OutliersFilled.PERIMETER);
title('perimeter');
subplot(3,3,3);
boxplot(OutliersFilled.MAJORAXIS);
title('majoraxis');
subplot(3,3,4);
boxplot(OutliersFilled.MINORAXIS);
title('minoraxis');
subplot(3,3,5);
boxplot(OutliersFilled.ECCENTRICITY);
title('eccentricity');
subplot(3,3,6);
boxplot(OutliersFilled.CONVEX_AREA);
title('convex area');
subplot(3,3,8);
boxplot(OutliersFilled.EXTENT);
title('extent');
%}

% NORMALISATION
Normalized = normalize(outliersFilled,'range');
fullData = [Normalized, randomisedData(:,8)];


%remove all outliers 
%for column = 1:7
   % for row = 1:size(fullData,1)
        %if row ==0|| row ==1

%NoOutliers = find(any(fullData == 0))


%boxplots for after normalisation
%{
figure;
subplot(3,3,1);
boxplot(fullData.AREA);
title('Area');
subplot(3,3,2);
boxplot(fullData.PERIMETER);
title('perimeter');
subplot(3,3,3);
boxplot(fullData.MAJORAXIS);
title('majoraxis');
subplot(3,3,4);
boxplot(fullData.MINORAXIS);
title('minoraxis');
subplot(3,3,5);
boxplot(fullData.ECCENTRICITY);
title('eccentricity');
subplot(3,3,6);
boxplot(fullData.CONVEX_AREA);
title('convex area');
subplot(3,3,8);
boxplot(fullData.EXTENT);
title('extent');
%}

%% Data partitioning

%testing sets
%variables to set data amounts for testing
TrainingAmount = 1/3;
InitialTesting = 1/3;
FinalTesting = 1/3;


tableSize = size(fullData,1); %number of datapoints in total

%creating tables with correct data

%we only use training and testingdata giving us a 50-50 training testing
%split
TrainingData = fullData(1:(TrainingAmount * tableSize),:) ;
TestingData = fullData(  ((TrainingAmount * tableSize)+1): (2*(TrainingAmount * tableSize)) ,:);
FinalData = fullData( ((2*(TrainingAmount * tableSize))+1):tableSize, :);


%Cross validation sets

%splitting data for cross validation
crossData = {};
for n=1:10 %splits FullData into ten separtate partitions
    crossData{n} = fullData(((n-1)*tableSize/10 +1):(n*(tableSize/10)),:);
end

%cross validating
averageRate = 0;
for n=1:10 
    training = [];
    %create each training group by adding all data to training except
    %validation data
    for i = 1:10
        if i == n
            ;
        else
            training = [training ; crossData{i}];
        end
    end
    %train model
    LinearModel = fitcsvm(training(:,1:7),training.CLASS, 'KernelFunction','linear','BoxConstraint',1 ,'KernelScale',1);
    output = predict(LinearModel,crossData{n});
    totalCorrect = 0;
    %check prediction rates
    for i = 1:size(crossData{n},1)
        if crossData{n}{i,"CLASS"} == output(i)
            totalCorrect = totalCorrect + 1;
        end
    end
    rate = totalCorrect/size(crossData{n},1) * 100;
    averageRate = averageRate + rate;
end
%average out each group
averageRate = averageRate / 10


%% creating SVMs


%creating a linear svm
LinearModel1 = fitcsvm(TrainingData(:,1:7),TrainingData.CLASS, 'KernelFunction','linear','BoxConstraint',1);
size(LinearModel1.SupportVectors);


[optimumObjectiveFunction, optimumBoxConstraint, optimumKernelScale] = hillClimbing(TrainingData, 100,0.01,0.01)

svm = createGaussianSVM(TrainingData,1,1);
objectiveFunction = resubLoss(svm)


TrainingDataPoly = fullData(1:150,:) ;

[optimumObjectiveFunction, optimumBoxConstraint, optimumKernelScale] = hillClimbingPoly(TrainingDataPoly, 100,1,0.01)




%predicting
%label = predict(GaussianModel,TestingData);






%% cross validating with optimum parameters

tableSize = size(TestingData,1);

crossData = {};
for n=1:10 %splits FullData into ten separtate partitions
    crossData{n} = TrainingData(((n-1)*tableSize/10 +1):(n*(tableSize/10)),:);
end

%cross validating
averageRate = 0;
for n=1:10 
    training = [];
    %create each training group by adding all data to training except
    %validation data
    for i = 1:10
        if i == n
            ;
        else
            training = [training ; crossData{i}];
        end
    end
    %train model
    svm = createGaussianSVM(training,optimumBoxConstraint, optimumKernelScale);
    output = predict(svm,crossData{n});
    totalCorrect = 0;
    %check prediction rates
    for i = 1:size(crossData{n},1)
        if crossData{n}{i,"CLASS"} == output(i)
            totalCorrect = totalCorrect + 1;
        end
    end
    rate = totalCorrect/size(crossData{n},1) * 100;
    averageRate = averageRate + rate;
end
%average out each group
averageRate = averageRate / 10






crossData = {};
for n=1:10 %splits FullData into ten separtate partitions
    crossData{n} = TestingData(((n-1)*tableSize/10 +1):(n*(tableSize/10)),:);
end

%cross validating
averageRate = 0;
for n=1:10 
    training = [];
    %create each training group by adding all data to training except
    %validation data
    for i = 1:10
        if i == n
            ;
        else
            training = [training ; crossData{i}];
        end
    end
    %train model
    svm = createPolySVM(training,optimumBoxConstraint, optimumKernelScale);
    size(svm.SupportVectors)
    output = predict(svm,crossData{n});
    totalCorrect = 0;
    %check prediction rates
    for i = 1:size(crossData{n},1)
        if crossData{n}{i,"CLASS"} == output(i)
            totalCorrect = totalCorrect + 1;
        end
    end
    rate = totalCorrect/size(crossData{n},1) * 100
    averageRate = averageRate + rate;
end
%average out each group
averageRate = averageRate / 10

%% Gaussian hill climbing

%function to create svms for testing
function gaussianSVM = createGaussianSVM(TrainingData,boxConstraint, KernelScale)
    gaussianSVM = fitcsvm(TrainingData(:,1:7),TrainingData.CLASS, 'KernelFunction','gaussian','BoxConstraint',boxConstraint,'KernelScale',KernelScale); 
end

%hill climbing function to find the optimal objective function
function [optimumObjective, optimumBox, optimumKernel] = hillClimbing(TrainingData, totalRuns, stepBox, stepKernel)
%inital values to check against
    intialBoxConstraint = 1;
    intialKernelScale = 1;
    bestBox = 1;
    bestKernel = 1;
    svm = createGaussianSVM(TrainingData,intialBoxConstraint,intialKernelScale);
    optimumObjective = resubLoss(svm);

    %loop for a set number of times to check if in local optimum
    for i = 1:totalRuns
        %random starting areas
        intialBoxConstraint = randi(1000);
        intialKernelScale = randi(1000);
        svm = createGaussianSVM(TrainingData,intialBoxConstraint,intialKernelScale);
        %starting value to check against
        oldObjectiveFunction = resubLoss(svm);
        %stopping local best equal old, so while loop does not instanlty
        %terminate
        localBest = oldObjectiveFunction + 1;
        
        %while the best we can reach is not the same as the old value
        %so we dont get stuck
        while oldObjectiveFunction ~= localBest  %checking if not equals matlab just has weird syntax
            %to see if we beat last time
            oldObjectiveFunction = localBest;

            % step all and check (experimenting to find most optimal move)
            svm = createGaussianSVM(TrainingData,intialBoxConstraint + stepBox, intialKernelScale);
            localBestA = resubLoss(svm);       
            
            svm = createGaussianSVM(TrainingData,intialBoxConstraint - stepBox, intialKernelScale);
            localBestB = resubLoss(svm);

            svm = createGaussianSVM(TrainingData,intialBoxConstraint, intialKernelScale + stepKernel);
            localBestC = resubLoss(svm);

            svm = createGaussianSVM(TrainingData,intialBoxConstraint, intialKernelScale - stepKernel);
            localBestD = resubLoss(svm);
            
            %see which step is the most sucessful
            if localBestA <= localBestB && localBestA <= localBestC && localBestA <= localBestD
                if localBestA <= localBest
                    localBest = localBestA;
                    intialBoxConstraint = intialBoxConstraint + 1;
                end 
            elseif localBestB <= localBestC && localBestB <= localBestD
                 if localBestB <= localBest
                    localBest = localBestB;
                    intialBoxConstraint = intialBoxConstraint - 1;
                end 
            elseif localBestC <= localBestD
                 if localBestC <= localBest
                    localBest = localBestC;
                    intialKernelScale = intialKernelScale + 1;
                end 
            else 
                 if localBestD <= localBest
                    localBest = localBestD;
                    intialKernelScale = intialKernelScale - 1;
                end 
            end 
        end
        
        %if the found optimum is better than previously found points
        if localBest < optimumObjective
            optimumObjective = localBest;
            optimumBox = intialBoxConstraint;
            optimumKernel = intialKernelScale;
        end        
    end
    
    %Gsvm = createGaussianSVM(TrainingData,1,1);
    %resubLoss(Gsvm)
    %size(Gsvm.SupportVectors)
    %Gsvm1 = createGaussianSVM(TrainingData,optimumBox,optimumKernel);
    %resubLoss(Gsvm1)
    %size(Gsvm1.SupportVectors)
end


%% Polynomial hill climbing
%function to create svms for testing
function polySVM = createPolySVM(TrainingData,boxConstraint, KernelScale)
    polySVM = fitcsvm(TrainingData(:,1:7),TrainingData.CLASS, 'KernelFunction','polynomial','BoxConstraint',boxConstraint,'PolynomialOrder',KernelScale); 
end

%hill climbing function to find the optimal objective function
function [optimumObjective, optimumBox, optimumKernel] = hillClimbingPoly(TrainingData, totalRuns, stepBox, stepKernel)
%inital values to check against
    intialBoxConstraint = 1;
    intialKernelScale = 1;
    svm = createPolySVM(TrainingData,intialBoxConstraint,intialKernelScale);
    optimumObjective = resubLoss(svm);
    %loop for a set number of times to check if in local optimum
    for i = 1:totalRuns
        %random starting areas
        intialBoxConstraint = randi(1000);
        intialKernelScale = randi(10); %lower than other because kept getting too high
        svm = createPolySVM(TrainingData,intialBoxConstraint,intialKernelScale);
        %starting value to check against
        oldObjectiveFunction = resubLoss(svm);
        %stopping local best equal old, so while loop does not instanlty
        %terminate
        localBest = oldObjectiveFunction + 1;
        
        %while the best we can reach is not the same as the old value
        %so we dont get stuck
        while oldObjectiveFunction ~= localBest  %checking if not equals matlab just has weird syntax
            %to see if we beat last time
            oldObjectiveFunction = localBest;
            if (stepKernel - 1) <= 0
                stepKernel =  stepKernel* 0.1;
                %break
            end
            if (stepBox - 1) <= 0
                stepBox =  stepBox* 0.1;
                %break
            end

            % step all and check (experimenting to find most optimal move)
            svm = createPolySVM(TrainingData,intialBoxConstraint + stepBox, intialKernelScale);
            localBestA = resubLoss(svm);       
            
            svm = createPolySVM(TrainingData,intialBoxConstraint - stepBox, intialKernelScale);
            localBestB = resubLoss(svm);

            svm = createPolySVM(TrainingData,intialBoxConstraint, intialKernelScale + stepKernel);
            localBestC = resubLoss(svm);

            svm = createPolySVM(TrainingData,intialBoxConstraint, intialKernelScale - stepKernel);
            localBestD = resubLoss(svm);
            
            %see which step is the most sucessful
            if localBestA <= localBestB && localBestA <= localBestC && localBestA <= localBestD
                if localBestA <= localBest && localBestA ~= 0
                    localBest = localBestA;
                    intialBoxConstraint = intialBoxConstraint + 1;
                end 
            elseif localBestB <= localBestC && localBestB <= localBestD
                 if localBestB <= localBest && localBestB ~= 0
                    localBest = localBestB;
                    intialBoxConstraint = intialBoxConstraint - 1;
                end 
            elseif localBestC <= localBestD
                 if localBestC <= localBest && localBestC ~= 0
                    localBest = localBestC;
                    intialKernelScale = intialKernelScale + 1;
                end 
            else 
                 if localBestD <= localBest && localBestD ~= 0
                    localBest = localBestD ;
                    intialKernelScale = intialKernelScale - 1;
                end 
            end            
        end
        
        %if the found optimum is better than previously found points
        if localBest < optimumObjective
            optimumObjective = localBest;
            optimumBox = intialBoxConstraint;
            optimumKernel = intialKernelScale;
        end        
    end
   Gsvm = createPolySVM(TrainingData,1,1);
   resubLoss(Gsvm)
   size(Gsvm.SupportVectors)
   Gsvm1 = createPolySVM(TrainingData,optimumBox,optimumKernel);
   resubLoss(Gsvm1)
   size(Gsvm1.SupportVectors)
end


